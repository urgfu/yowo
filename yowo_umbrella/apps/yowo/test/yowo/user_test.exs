defmodule YOWO.UserTest do
	use ExUnit.Case, async: true

	setup do
		{:ok, user} = YOWO.User.start_link(:fakePID, [1,2,3], [])
		{:ok, user: user}
	end

	test "sets a user's grids", %{user: user} do
		assert YOWO.User.get_grids(user) == [1,2,3]
		
		YOWO.User.set_grids(user, [4,5,6])
		assert YOWO.User.get_grids(user) == [4,5,6]

	end

	test "searches user for specific grid", %{user: user} do
		YOWO.User.set_grids(user, [1, 2, 3])
		assert YOWO.User.has_grid(user, 3) == true
		assert YOWO.User.has_grid(user, 4) == false
	end

	test "sets a user's connection", %{user: user} do
		YOWO.User.set_connection(user, :newPID)
		assert YOWO.User.get_connection(user) == :newPID
	end


end