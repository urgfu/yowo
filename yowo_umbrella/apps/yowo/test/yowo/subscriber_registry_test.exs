defmodule YOWO.SubscriberRegistryTest do
	use ExUnit.Case, async: true

	# create forwarder to test events
	defmodule Forwarder do
		use GenEvent

		def handle_event(event, parent) do
			# note: all we're doing is sending to the parent...
			# no reason you can't make a handler to send somewhere else...
			# or somewhere elseS!!!
			send parent, event
			{:ok, parent}
		end

	end

	setup do
		# start an event manager
		{:ok, manager} = GenEvent.start_link
		# start our user supervisor
		{:ok, user_sup} = YOWO.User.Supervisor.start_link
		# start the registry
		{:ok, sub} = YOWO.SubscriberRegistry.start_link(user_sup, manager)
		# add a monitored event handler (Forwarder) to our event Manager
		GenEvent.add_mon_handler(manager, Forwarder, self())
		# create test user
		user = {1, :fakeConnection, [1,2,3], []}

		{:ok, sub: sub, user: user}
	end

	test "adds a new subscriber", %{sub: sub, user: user} do
		# make sure it doesn't exist yet
		assert YOWO.SubscriberRegistry.get_subscriber(sub, 1) == :error
		# add the user
		{_userID, pid} = YOWO.SubscriberRegistry.add_subscriber(sub, user)
		# check
		assert_receive {:created_subscriber, 1, ^pid}
		assert YOWO.SubscriberRegistry.get_subscriber(sub, 1) == {1, pid}
	end

	test "removes a subscriber", %{sub: sub, user: user} do
		# try to remove a subscriber that isn't there
		assert YOWO.SubscriberRegistry.remove_subscriber(sub, 1) == :user_not_found

		# add the user
		{userID, pid} = YOWO.SubscriberRegistry.add_subscriber(sub, user)
		# make sure it's there
		assert YOWO.SubscriberRegistry.get_subscriber(sub, userID) == {userID, pid}
		# remove the user
		assert YOWO.SubscriberRegistry.remove_subscriber(sub, userID) == {:stopping, pid}
		assert Process.alive?(pid) == false

		# wait for signal once we add event manager
		assert_receive {:removed_subscriber, userID, ^pid}
		# then check that get_subscriber returns :error
		assert YOWO.SubscriberRegistry.get_subscriber(sub, userID) == :error

	end

	test "removes a subscriber on crash", %{sub: sub, user: user} do
		{userID, pid} = YOWO.SubscriberRegistry.add_subscriber(sub, user)
		assert YOWO.SubscriberRegistry.get_subscriber(sub, userID) == {userID, pid}
		# crash the process harshly!
		Process.exit(pid, :shutdown)
		# make sure the subscriber was removed
		assert_receive {:removed_subscriber, _userID, ^pid}

	end

end