Yowo
====

# General Idea

This is the main application. 

## Subscribers
* Adding, updating, removing, and looking up of subscribers (w/connections ultimately) and grids.
* probably in ETS for restore on crash...
* Need to decide how to distribute, either based on performance on consistent hashing algo, but for now keep it to one node only.

## Events 
* Needs to handle receving events, pushing to proper subscribers, and storing. 
* It also needs to check for applicable events when subscribers connect or move.
* Decision: Redis, more permanent DB, or pure ETS.
	* probably Redis for now, with possibility of moving to 
	* generally, start with a simple HashDict or map...
		* Build upon this as you progress, like the Mix OTP tutorial.
* Need to notify on new events, new subscribers, subscriber moving grids
* possibly 2 event handlers

### Event Handlers
* these are really what you create.
* then, {:ok, pid} = GenEvent.start_link
* then, add_mon_handler -> basically, you add a supervised handler to the event manager. now it will do what you
*    decided in handle event.
* then you pass this event handler to the subscriber registry! (and vice versa)
* that's how you can post across events



## Server
* Should this stay in its own app? Probably. Server can just push the subscriber / connection events to this module, as in the Commands from OTP...
* These will be supervised tasks.
* Worry about this second.


====

# Actual Work Items

## SubscriberManager - GenServer - closeset analog is our Registry
* Store in ETS
* Ultimately, need to figure out distribution/ routing
* Should these store pid of server agent? maybe think of each subscriber as a bucket of grids?
	* at some point, need to pass the pid to a subscriber registry, must be detectable by grid
	* either that or just pass the user and the pid of a server to user registry, that then pushes again to the actual server
		* may be overkill?
	* this can be a command by the server that calls module functions for add subscriber...
* try to use or plan a hot swap with the code_change callback as well!!!

## SubscriberEvents - GenEvent
* adding of subscriber (needs to fetch events)
* moving of subscriber (needs to look for any events to pull)
* is GenEvent necessary or should we just dispatch tasks?
	* see debate under the Event Manager -> seems like a good idea to use them, think of as forwarder
* So, we publish the subscriber actions, then the event manager (GenServer) receives through HandleInfo

## EventManager - GenServer - like another Registry
* maintain state of events, location, etc.


## Event..Events - GenEvent 
Will be notified on:
* creation of event
	* needs to push to connected subscribers
	* needs to push to Redis / Permanent Storage
* is this necessary? 
	* may not do the actual pushing, but at least can be fired off when we need to await
	* may actually be good to do the push! this way we reduce load on GenServer (Events or Subscribers) by only forwarding once
		* also better than creating short-lived tasks!
	* so, these might actually be a nifty idea for the event pushing, separating it from the agents that handle state! ***************
		* really, think of as an Event Forwarder - use async calls, they'll just pass the appropriate functions to the right processes!
* can (and will!) be supervised as well
* maybe this publish creation (and /or removal) of events, let the SubscriberManagers pick up in handle info

* so, we cross-publish the events to the opp managers/servers?

# Question Items
* how to handle expiring events in ETS
	* or do we only store in Redis and just pull active? hmm...
	* need to look at Redis ability to schedule moving of expired events out of active pool, rather than just setting a flag...
* understanding of GenEvent and monitors needs the mst work...
* how to handle not sending duplicate events when a user moves

