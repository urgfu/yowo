defmodule YOWO do
	use Application

	# start supervisor here
	def start(_type, _args) do
		YOWO.Supervisor.start_link
	end
end
