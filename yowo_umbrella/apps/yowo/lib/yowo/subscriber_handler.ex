defmodule YOWO.SubscriberHandler do
	use GenEvent

	def handle_event({:created_subscriber, userID, pid}, state) do
		IO.puts "subscriber handler received create"
		# TODO: need to grab events or call event registry
		{:ok, state}
	end

	
	def handle_event(_msg, state) do
		# avoid death
		{:ok, state}
	end

end