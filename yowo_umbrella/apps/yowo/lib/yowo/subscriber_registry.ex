defmodule YOWO.SubscriberRegistry do
	use GenServer

	##----------------------- Client API (from outside the agent) ---------------------------------

	def start_link(user_sup, manager, opts \\ []) do
		# start the server
		GenServer.start_link(__MODULE__, {user_sup, manager}, opts)
	end

	def add_subscriber(server, user) do
		# remember, a user is {userID, connection_pid, grids}
		GenServer.call(server, {:new_subscriber, user})
	end

	def get_subscriber(server, userID) do
		# really just for debugging
		GenServer.call(server, {:get_subscriber, userID})
	end

	def update_grids(server, userID, grids) do
		GenServer.call(server, {:update_grids, userID, grids})
	end

	def remove_subscriber(server, userID) do
		GenServer.call(server, {:remove_subscriber, userID})
	end

	##----------------------------------- Server callbacks ------------------------------------------

	def init({user_sup, manager}) do
		# TODO: make sure this is resilient (for multiple...)
		start_handler(manager)
		# TODO: recover any connections in ets and re-monitor them
		{:ok, %{users: HashDict.new, monitors: HashDict.new, user_sup: user_sup, manager: manager}}
	end

	def start_handler(manager) do
		GenEvent.add_mon_handler(manager, YOWO.SubscriberHandler, self())
	end

	def handle_call({:new_subscriber, {userID, connection_pid, grids, events}}, _from, state) do
		# our hash dict (and ultiamtely ets) will use the userID as a key, and the pid of the AGENT of the user as the value
		case HashDict.fetch(state.users, userID) do
			{:ok, userAgent} -> {:reply, {userID, userAgent}, state}
			# the subscriber exists -> simply reply with the current user agent
			:error ->
				# need to start a new agent for this subscriber
				{:ok, pid} = YOWO.User.Supervisor.start_user(state.user_sup, {connection_pid, grids, events})
				users = HashDict.put(state.users, userID, pid)

				# make sure we monitor the user agent, so we can remove this user when they disconnect later
				monitor = Process.monitor(pid)
				monitors = HashDict.put(state.monitors, monitor, userID)

				# push notification, pid, and the user's connection_pid on create to Subscriber's EventManager (MUST!), include in state
				# the EventManager will look for any events to push, rather than looking for them here
				# ultimately, may need to pass along a user's events as well to avoid duplicates
				GenEvent.ack_notify(state.manager, {:created_subscriber, userID, pid})

				# reply for backpressure and loop
				{:reply, {userID, pid}, %{state | users: users, monitors: monitors}}
		end	
	end

	def handle_call({:update_grids, userID, grids}, _from, state) do
		IO.puts "handle call update grids"
		# try to fetch the user process
		case HashDict.fetch(state.users, userID) do
			# update the grids
			{:ok, userAgent} -> 
				YOWO.User.set_grids(userAgent, grids)
				# send to EventManager!!! look for any new events to push
				GenEvent.ack_notify(state.manager, {:subscriber_moved, userID})
				{:reply, :ok, state}
			:error ->
				{:reply, :no_user, state}
		end
		
	end

	
	def handle_call({:get_subscriber, userID}, _from, state) do
		case HashDict.fetch(state.users, userID) do
			{:ok, userAgent} -> {:reply, {userID, userAgent}, state}
			:error -> {:reply, :error, state}
		end
	end


	def handle_call({:remove_subscriber, userID}, _from, state) do
	# this may need to be synchronous, so we don't make the cast, then they reconnect, then we remove!
		case HashDict.get(state.users, userID) do
			nil -> {:reply, :user_not_found, state}
			userAgent ->
				# TODO: need to check first in case it's not there...
				# kill the agent and let the monitor handle state
				Agent.stop(userAgent)
				{:reply, {:stopping, userAgent}, state}
		end
	end

	@doc """
	This should handle receiving new events. Find users with that grid that don't already have this event. And push!
	"""
	def handle_cast({:push_event, event}, state) do
		IO.puts "handle_info push event"
		# event will be a map of eventID, catID, timespanID, start, end, descr, grid
		# let each agent handle the heavy lifting!
		# or should we just return the userAgent processes??? the great debate...
		# may also need to report that the user received the event, so we can track
		# that could be a notify
		# this seems expensive...
		state.users
		|> HashDict.values
		|> Enum.each(fn pid -> YOWO.User.push_event(pid, event) end)
		{:noreply, state}
	end


	def handle_info({:DOWN, monitor, :process, pid, _reason}, state) do
		# Handle downed user agents (note: this is the user managing agent, NOT the connection!)
		# But if we link the connection... can we ensure they both go down?
		IO.puts "handle_info process went down"

		{userID, monitors} = HashDict.pop(state.monitors, monitor)
		users = HashDict.delete(state.users, userID)
		# send event
		GenEvent.sync_notify(state.manager, {:removed_subscriber, userID, pid})
		# return new state
		{:noreply, %{state | users: users, monitors: monitors}}
		
	end

	def handle_info({:gen_event_EXIT, handler, reason}, manager) do
		# this means the subscriber handler crashed
		IO.puts "received exit for handler"
		IO.puts "restarting!!!"
		# TODO: get this working!!!
		#start_handler(manager)

		{:noreply, manager}
	end

	def handle_info(_msg, state) do
		# handle unknown messages
		{:noreply, state}
	end
end
