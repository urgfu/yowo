defmodule YOWO.User.Supervisor do
	use Supervisor

	def start_link(opts \\ []) do
		Supervisor.start_link(__MODULE__, :ok, opts)
	end

	def start_user(supervisor, {connection_pid, grids, events}) do
		Supervisor.start_child(supervisor, [connection_pid, grids, events])
	end

	def init(:ok) do
		children = [
			worker(YOWO.User, [], restart: :temporary)
		]

		supervise(children, strategy: :simple_one_for_one)
	end
end