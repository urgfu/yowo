defmodule YOWO.User do
	@doc """
	Starts an agent that represents a connected user.
	Main function is to handle movement / grid changes.
	Must be initailized with connection process. (note, may be updated to just userID)
	"""
	def start_link(pid, grids, events) do
		Agent.start_link(fn -> %{:connection => pid, :grids => grids, :events => events} end)
	end


	@doc """
	Gets the grid of a user.
	"""
	def get_grids(user) do
		# note: using Map.get is safer if key has not yet been set
		Agent.get(user, &Map.get(&1, :grids))
	end

	@doc """
	Sets the grids for a user.
	"""
	def set_grids(user, grids) do
		Agent.update(user, &Map.put(&1, :grids, grids))
	end


	@doc """
	Returns true when a user is subscribed to a certain grid.
	"""
	def has_grid(user, grid) do
		Agent.get(user, &Map.get(&1, :grids))
		|> Enum.any?(fn g -> g == grid end)
	end

	@doc """
	Adds a new event to the user's event list.
	"""
	def add_event(user, eventID) do
		Agent.update(user, &Map.update(&1, :events, [], fn e -> [eventID|e] end))
	end

	@doc """
	Returns whether or not a user already has this event.
	"""
	def has_event(user, eventID) do
		Agent.get(user, &Map.get(&1, :events))
		|> Enum.any?(fn e -> e == eventID end)
	end

	@doc """
	Gets a user's connection process.
	"""
	def get_connection(user) do
		Agent.get(user, &Map.get(&1, :connection))
	end

	@doc """
	Sets a user's connection process.
	"""
	def set_connection(user, pid) do
		Agent.update(user, &Map.put(&1, :connection, pid))
	end

	def push_event(user, event) do
		if !Agent.has_event(user, event.eventID) do
			_pid = Agent.get_connection(user)
			# TODO: push!!!

			# update the events
			Agent.add_event(user, event.eventID)
		end
	end



end