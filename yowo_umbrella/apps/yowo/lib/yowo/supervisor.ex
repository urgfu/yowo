defmodule YOWO.Supervisor do
	use Supervisor

	def start_link do
		Supervisor.start_link(__MODULE__, :ok)
	end

	# named processes
	@manager YOWO.SubscriberManager
	@handler YOWO.SubscriberHandler
	@sub_registry YOWO.SubscriberRegistry
	@user_sup YOWO.User.Supervisor


	def init(:ok) do
		children = [
			# add supervisors and workers
			worker(GenEvent, [[name: @manager]]),
			supervisor(YOWO.User.Supervisor, [[name: @user_sup]]),
			worker(YOWO.SubscriberRegistry, [@user_sup, @manager, [name: @sub_registry]])
		]

		# TODO: need to evaluate strategy
		supervise(children, strategy: :one_for_one)
	end
end