﻿(* --------------- YoWo conceptual model ---------------------------------- *)
 
(*  The idea is to start with basic events and feeds. 
 *  Post a few events, and have the appropriate users receive them.
 *  The users will move into adjacent grids and should receive any new notifications
 *  If they go back, they should not receive duplicates.
 *)

(* --------------- TODO -------------------------
 * Separate the client-side user from the server-side one
       * don't want real instance in the subManager
       * inaccurate because we're picking up grid change before we actually pass the msg
 * Map Reduce the subscriber find-by-grid
       * don't want too many in a subscriber manager -> instead distribute
       * each sub man will filter by grid
       * concatenate the results in pub service
 * Sockets
       * Need to have the UserFeed know when connection breaks -> call unsubscribe and die gracefully
       * Need to have the cs_user know when connected vs. not
 * Resource Manager / Distributed / Cross Process
       * ultimate model should be distributed. Consider Akka.NET or MBrace or Fsharp.CloudAgent? or hand roll
       * Right now getting away with passing whole agents around, as opposed to ProcessIDs. Not sure how to distribute this ultimately...
 * Supervisors and error handling
 * Adjacent Grids
 * Mock storing events on cs_user (like the map)
 * Mock the event expiration on cs_user and in storage
 * Mock the persistant storage
       * need to get old active events when a user changes grid
       * need to push when events expire? or should this be handled on cs...
 * Write unit tests for getting feeds, creating events, posts, movement, etc.
       * preserve tests for later on during actual modelling
 * F# formatting for docs
 * Mono container? simulate hosting. Or just look into Azure?
 *)

open System
open System.Net.Sockets


/// basic record types
type Point = {
    latitude: int;
    longitude: int
}

type Event = {
    eventID: int;
    posterID: int;
    description: string;    
    startTime: DateTime;
    endTime: DateTime;
    grid: string
}

(* Time choices -> should be simple and fun
 * Don't want users entering specific times, takes to long, distracting
 *)
type TimeChoice =
    | FewMinutes
    | FewHours
    | OneDay


type User = {
    id: int;
    name: string;
    mutable location: Point; // don't really need on server side in final, just on client side to determine grids, can be immutable there
    mutable grid: string // will replace with grids[9], outer array not mutable
    // ultimately the server instance needs some sort of reference to its socket connection for publishing
}

(* Grids and Feeds
 * A grid is just an area in space, determined by certain lat and long
 * It will be used to appropriate feeds
 * The "grid" is functionally just an identifier that will indicate pub topics
 * Will need real rules for mapping grid to lat and long, but for now using some even numbers
 * E.g. grid1 is a square {0,0} to {10,10}, grid2 is {10,0} to {20,10}
 *)

// some mapping between location and grid identifier
let findGrid (point:Point) = 
    let findlower10 num = 
        (num / 10) * 10
    let x = findlower10 point.latitude
    let y = findlower10 point.longitude
    sprintf "%i_%i" x y

// this just represents an auto-incerementing ID for demo purposes
let mutable eventSerial = 1
let newID() = 
    eventSerial <- (eventSerial + 1)
    eventSerial

// forming an event
let formEvent user desc timeChoice = 
    let d = DateTime.Now
    
    let findEndTime timeChoice = 
        match timeChoice with
        | FewMinutes -> d.AddMinutes(float 10)
        | FewHours -> d.AddHours(float 3)
        | OneDay -> d.AddDays(float 1)

    let event = {
        eventID = newID();
        posterID = user.id;
        description = desc;
        startTime = d;
        endTime = findEndTime timeChoice;
        grid = user.grid
    }
    event


(* UserFeed: this is the main connection for a user to a socket.  
 * The issue will be storing a reference to this agent inside the subscriber list.
 * Think Erlang's process ID. We need to know which agent to post to.
 *)
type UserFeed (client:TcpClient) = 
    let agent = MailboxProcessor.Start(fun inbox ->
        let stream = client.GetStream() // here or in loop?
        // might need the while loop so it disconnects...
        let messageLoop() = async {
            while true do
                let! msg = inbox.Receive()
                //let stream = client.GetStream()
                do! stream.AsyncWrite(msg)
        }
        messageLoop()
    )
    member this.Post msg = agent.Post msg

(* MockUserFeed: like the user feed, but for test purposes skipping the socket part.
 * Just working on passing the reference to the subscriber list.
 * Needs a way to unsubscribe when disconnecting, e.g. loop ends. 
 * Might need to include a ref to the Subscriber Manager, but creates circular dep... hmm...
 * Maybe the UserFeed holds the only ref to sub man? return channel on post? seems backwards...
 * Would each user have a whole feed like this w/same client? Or would there be one feed...
 *)

 type UserMessage =
    | Post of Event
    | Disconnect

 type MockUserFeed (user, unsubscriber) = 
    let agent = MailboxProcessor.Start(fun inbox ->
        let messageLoop() = async {
            while true do
                let! msg = inbox.Receive()
                match msg with
                | Post event-> printfn "User Feed for %s received %s" user.name event.description
                | Disconnect -> unsubscriber()
            //let unsub = Unsubscribe(user.id)
            //sm.Post(unsub)
            unsubscriber()
        }
        messageLoop()
    )
    member this.Post msg = agent.Post msg

    
type UserReplyInfo = {
    user: User;
    uf: MockUserFeed
}

 
/// messages to send to SubscriberManager
type SubMessage =     
    | Connect of UserReplyInfo
    | Subscribe of User
    | Unsubscribe of int
    | Retrieve of string * AsyncReplyChannel<UserReplyInfo list>

(* SubscriberManager -> a filtering actor / agent to track users and topics 
 * Should include some sort of reference to the user's return channel, so we know how to broadcast
 * This may ultimately be some sort of processID, unless we can pass the agent itself
 *)
type SubscriberManager() = 
    // subscribe -> update user with grid
    let subscribe user subscribers = 
        // really, user should exist already, but in case they disconnected in between...
        let oldUser = subscribers |> Seq.tryFind(fun s -> s.user.id = user.id)
        match oldUser with
        | Some(u) -> match u.user.grid with
                     | g when g = user.grid -> subscribers // no need to update
                     | _ -> let old = 
                                subscribers
                                |> List.filter (fun u -> u.user.id <> user.id)
                            {user= user; uf= u.uf}::old
        | None -> subscribers // user no longer connected anyway

    // connect -> insert with UserFeed, then subscrie to grid
    let connect uri subscribers = 
        // really, user should not exist, but might as well be precautionary
        let oldUser = subscribers |> Seq.tryFind(fun s -> s.user.id = uri.user.id) 
        match oldUser with
        | Some(u) -> subscribe uri.user subscribers
        | None -> uri::subscribers

    // unsubscribe -> remove if exists
    let unsubscribe id subscribers = 
        subscribers |> List.filter(fun uri -> uri.user.id <> id)

    // retrieve -> bring back all users with that grid / topic
    let retrieve grid subscribers = 
        subscribers |> List.filter(fun uri -> uri.user.grid = grid)

    // subscribers should maintain Isolated State!!! async, but not concurrent access!!! No manual locking!!!
    let agent = MailboxProcessor.Start(fun inbox ->
        let rec messageLoop subscribers = async {
            let! eventMsg = inbox.Receive()
            let newSubscribers = 
                match eventMsg with
                | Connect uri -> connect uri subscribers
                | Subscribe user -> subscribe user subscribers
                | Unsubscribe id -> unsubscribe id subscribers
                | Retrieve (grid, replyChannel) -> replyChannel.Reply(retrieve grid subscribers)
                                                   subscribers
            return! messageLoop newSubscribers
        }
        messageLoop []
    )

    member this.Post msg = agent.Post msg
    member this.PostAndAsyncReply msg = agent.PostAndAsyncReply msg

(* Publishing service for pulling the topic-based subscribers and handling event broadcasting.
 * Should be ignorant and scalable.
 * Pass topics to subscriber managers (which will map), then reduce results.
 * Then broadcast.
 * Ultimately could have a resource manager, decide which PublisherService to hit
 *)
type PublisherService(subManager: SubscriberManager) =

    let pushToUser event uri = 
        let _post = Post(event)
        uri.uf.Post(_post)

    let agent = MailboxProcessor<Event>.Start(fun inbox ->
        let rec publishMessages() = async {
            let! event = inbox.Receive()
            // get users -> ultimately will reduce from multiple subscriber managers
            let! urs = subManager.PostAndAsyncReply(fun r -> Retrieve(event.grid, r))
            // TODO: publish -> ultimately pass through socket
            // for now, let's see if we can call the user's agent.
            urs
            |> Seq.iter (pushToUser event)
            return! publishMessages()
        }
        publishMessages()
    )
    member this.Post event = agent.Post event

(* This represents our initial event receiving queue
 * The Publisher Service is passed here. In reality, will be returned by a resource manager.
 *)

type EventProcessor(pubservice: PublisherService ) = 
    let agent = MailboxProcessor.Start(fun inbox ->
        let rec messageLoop() = async{
            let! eventMsg = inbox.Receive()
            printfn "Event Processor received %s" eventMsg.description
            // TODO: here would send to storage
            // send to publisher service
            pubservice.Post eventMsg
            return! messageLoop()
        }
        messageLoop()
    )
    member this.PostEvent msg = agent.Post msg


(*------------ Message Posting -----------------*)

// posting an event
let postEvent user desc timeChoice (eventProcessor: EventProcessor) = 
    let event = formEvent user desc timeChoice
    eventProcessor.PostEvent event

// connect a user for first time
let connect user (sm: SubscriberManager) = 
    // need disconnect function to pass to uf?
    let unsubMsg = Unsubscribe(user.id)
    let unsubFunc() = sm.Post(unsubMsg)
    let uf = MockUserFeed(user, unsubFunc)
    let uri = {user = user; uf = uf}
    let conn = Connect(uri)
    sm.Post(conn)

// change grid -> subscribe to new topic
let changeGrid user (sm: SubscriberManager) = 
    let sub = Subscribe(user)
    sm.Post(sub)

// manual disconnect -> should only occur when stream breaks, but maybe want to mock it for modelling purposes
// note: we have no ref to this feed, so kinda useless
let disconnect (uf:MockUserFeed) = 
    let dis = Disconnect
    uf.Post(dis)


(*-------------- Run the Model -----------------*)

// start the model
let sm = SubscriberManager()
let ps = PublisherService(sm)
let ep = EventProcessor(ps)

// currying a post function with current instance of Event Processor for ease of modelling
let makePost user desc timeChoice = postEvent user desc timeChoice ep

// create some users
let A = {id = 1; name = "Elliott"; location = {latitude = 1; longitude = 1}; grid = ""}
let B = {id = 2; name = "Sufjan"; location = {latitude = 5; longitude = 7}; grid = ""}
let C = {id = 3; name = "Margot"; location = {latitude = 12; longitude = 30}; grid = ""}

// find their current grids -> A and B are in same grid
A.grid <- findGrid A.location
B.grid <- findGrid B.location
C.grid <- findGrid C.location

printfn "Initial Grids: A: %s, B: %s, C: %s" A.grid B.grid C.grid

connect A sm
connect B sm
connect C sm

// A posts an event
makePost A "Hello, World!" FewMinutes
makePost B "Hello again!" FewHours

// B changes grid
B.grid <- findGrid {latitude = 17; longitude = 33}
changeGrid B sm
makePost B "What up guys" FewMinutes
makePost C "hello?" FewMinutes

// C changes grid
C.grid <- findGrid {latitude = 5; longitude = 5}
changeGrid C sm
makePost C "who's there now?" FewHours
makePost A "I am!!!" FewMinutes

// B joins the party
B.grid <- findGrid {latitude = 1; longitude = 1}
changeGrid B sm
makePost B "Hey there all!" FewHours
makePost C "Hi!" FewMinutes
makePost A "Yo!" FewMinutes


// testing users...
//async {
//let! us = sm.PostAndAsyncReply(fun r -> Retrieve("0_0", r))
//us |> Seq.iter (fun u -> printfn "%s" u.name)
//} |> Async.Start


