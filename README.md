# README

Yowo App. Will push local, temporary events to users. Backend in Elixir. Frontend TBD. 

Currently the umbrella app contains two projects, yowo and yowo_server. Yowo is the main application. Yowo_server will handle the TCP connections and depend on yowo.

See inner project READMEs for specific details. 